import {Plugin} from 'ckeditor5/src/core';
import {toWidget, Widget} from 'ckeditor5/src/widget';
import HubspotFormCommand from './hubspotformcommand';

/**
 * HubspotForm editing functionality.
 */
export default class HubspotFormEditing extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [Widget];
  }

  /**
   * @inheritdoc
   */
  init() {
    this.attrs = {
      HubspotFormId: 'data-form-id',
      HubspotFormPortalId: 'data-portal-id',
    };
    const options = this.editor.config.get('HubspotForm');
    if (!options) {
      return;
    }
    const {previewURL, themeError} = options;
    this.previewUrl = previewURL;
    this.themeError =
      themeError ||
      `
      <p>${Drupal.t(
        'An error occurred while trying to preview the Hubspot Form. Please save your work and reload this page.',
      )}<p>
    `;

    this._defineSchema();
    this._defineConverters();

    this.editor.commands.add(
      'HubspotForm',
      new HubspotFormCommand(this.editor),
    );
  }

  /**
   * Fetches the preview.
   */
  async _fetchPreview(modelElement) {
    const query = {
      hubspot_form_id: modelElement.getAttribute('HubspotFormId'),
      hubspot_portal_id: modelElement.getAttribute('HubspotFormPortalId'),
    };
    const response = await fetch(
      `${this.previewUrl}?${new URLSearchParams(query)}`
    );
    if (response.ok) {
      return await response.text();
    }

    return this.themeError;
  }

  /**
   * Registers HubspotForm as a block element in the DOM converter.
   */
  _defineSchema() {
    const schema = this.editor.model.schema;
    schema.register('HubspotForm', {
      allowWhere: '$block',
      isObject: true,
      isContent: true,
      isBlock: true,
      allowAttributes: Object.keys(this.attrs),
    });
    this.editor.editing.view.domConverter.blockElements.push('hubspotform');
  }

  /**
   * Defines handling of drupal media element in the content lifecycle.
   *
   * @private
   */
  _defineConverters() {
    const conversion = this.editor.conversion;

    conversion
      .for('upcast')
      .elementToElement({
        model: 'HubspotForm',
        view: {
          name: 'hubspotform',
        },
      });

    conversion
      .for('dataDowncast')
      .elementToElement({
        model: 'HubspotForm',
        view: {
          name: 'hubspotform',
        },
      });
    conversion
      .for('editingDowncast')
      .elementToElement({
        model: 'HubspotForm',
        view: (modelElement, {writer}) => {
          const container = writer.createContainerElement('figure');
          return toWidget(container, writer, {
            label: Drupal.t('HubspotForm'),
          });

        },
      })
      .add((dispatcher) => {
        const converter = (event, data, conversionApi) => {
          const viewWriter = conversionApi.writer;
          const modelElement = data.item;
          const container = conversionApi.mapper.toViewElement(data.item);
          const HubspotForm = viewWriter.createRawElement('div', {
            'data-hubspotform-preview': 'loading',
            'class': 'hubspotform-preview'
          });
          viewWriter.insert(viewWriter.createPositionAt(container, 0), HubspotForm);
          this._fetchPreview(modelElement).then((preview) => {
            if (!HubspotForm) {
              return;
            }
            this.editor.editing.view.change((writer) => {
              const HubspotFormPreview = writer.createRawElement(
                'div',
                {'class': 'hubspotform-preview', 'data-hubspotform-preview': 'ready'},
                (domElement) => {
                  domElement.innerHTML = preview;
                },
              );
              writer.insert(writer.createPositionBefore(HubspotForm), HubspotFormPreview);
              writer.remove(HubspotForm);
            });
          });
        };
        dispatcher.on('attribute:HubspotFormId:HubspotForm', converter);
        return dispatcher;
      });

    Object.keys(this.attrs).forEach((modelKey) => {
      const attributeMapping = {
        model: {
          key: modelKey,
          name: 'HubspotForm',
        },
        view: {
          name: 'hubspotform',
          key: this.attrs[modelKey],
        },
      };
      conversion.for('dataDowncast').attributeToAttribute(attributeMapping);
      conversion.for('upcast').attributeToAttribute(attributeMapping);
    });
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'HubspotFormEditing';
  }
}
