import { Plugin } from 'ckeditor5/src/core';
import HubspotFormEditing from './hubspotformediting';
import HubspotFormUI from './hubspotformui';

/**
 * Main entry point to the HubspotForm.
 */
export default class HubspotForm extends Plugin {

  /**
   * @inheritdoc
   */
  static get requires() {
    return [
      HubspotFormEditing,
      HubspotFormUI
    ];
  }

  /**
   * @inheritdoc
   */
  static get pluginName() {
    return 'HubspotForm';
  }
}
