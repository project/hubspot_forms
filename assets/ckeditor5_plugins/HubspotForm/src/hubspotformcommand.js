import { Command } from 'ckeditor5/src/core';

/**
 * Creates HubspotForm
 */
function createHubspotForm(writer, attributes) {
  return writer.createElement('HubspotForm', attributes);
}

/**
 * Command for inserting <hubspotform> tag into ckeditor.
 */
export default class HubspotFormCommand extends Command {
  execute(attributes) {
    const HubspotFormEditing = this.editor.plugins.get('HubspotFormEditing');
    const dataAttributeMapping = Object.entries(HubspotFormEditing.attrs).reduce(
      (result, [key, value]) => {
        result[value] = key;
        return result;
      },
      {},
    );
    const modelAttributes = Object.keys(attributes).reduce(
      (result, attribute) => {
        if (dataAttributeMapping[attribute]) {
          result[dataAttributeMapping[attribute]] = attributes[attribute];
        }
        return result;
      },
      {},
    );

    this.editor.model.change((writer) => {
      this.editor.model.insertContent(
        createHubspotForm(writer, modelAttributes),
      );
    });
  }

  refresh() {
    const model = this.editor.model;
    const selection = model.document.selection;
    const allowedIn = model.schema.findAllowedParent(
      selection.getFirstPosition(),
      'HubspotForm',
    );
    this.isEnabled = allowedIn !== null;
  }
}
