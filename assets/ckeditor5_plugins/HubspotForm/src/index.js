import HubspotForm from "./hubspotform";

/**
 * @private
 */
export default {
  HubspotForm: HubspotForm
};
