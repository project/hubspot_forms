<?php

namespace Drupal\hubspot_forms\Controller;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\editor\Entity\Editor;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\hubspot_forms\HubspotFormsCore;

/**
 * Returns the preview for Hubspot Form.
 */
class CKEditor5Preview extends ControllerBase {

  use StringTranslationTrait;

  /**
   * The renderer service.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer')
    );
  }

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   */
  public function __construct(Renderer $renderer) {
    $this->renderer = $renderer;
  }

  /**
   * Controller callback that renders the preview for CKeditor.
   */
  public function preview(Request $request, Editor $editor) {
    $hubspot_form_id = Xss::filter($request->query->get('hubspot_form_id'));
    $hubspot_portal_id = Xss::filter($request->query->get('hubspot_portal_id'));

    try {
      if (!$hubspot_form_id) {
        throw new \Exception();
      }
      $hubspotFormsCore = new HubspotFormsCore();
      $forms = $hubspotFormsCore->getFormIds();
      $key = $hubspot_portal_id . '::' . $hubspot_form_id;

      $build = [
        '#type' => 'inline_template',
        '#template' => '<div class="preview-hubspotform-label">{{ label }}</div>
          <div class="hubspotform-preview-wrapper">
          {% if form_name %}
            <div class="preview-hubspotform-item preview-hubspotform-name"><span>{{ form_name }}</span></div>
          {% else %}
            <div class="preview-hubspotform-item preview-hubspotform-name-na">{{ form_name_na }}</div>
          {% endif %}
          </div>',
        '#context' => [
          'label' => $this->t('Hubspot Form'),
          'form_name' => !empty($forms[$key]) ? $forms[$key] : '',
          'form_name_na' => $this->t('Invalid form ID (not found in your account)'),
        ],
      ];
    }
    catch (\Exception $e) {
      $build = [
        'markup' => [
          '#type' => 'markup',
          '#markup' => $this->t('Incorrect configuration for HubspotForm.'),
        ],
      ];
    }
    $renderer = \Drupal::service('renderer');
    return new Response($renderer->renderRoot($build));
  }

  /**
   * Access callback for viewing the preview.
   *
   * @param \Drupal\editor\Entity\Editor $editor
   *   The editor.
   * @param \Drupal\Core\Session\AccountProxy $account
   *   The current user.
   *
   * @return \Drupal\Core\Access\AccessResult|\Drupal\Core\Access\AccessResultReasonInterface
   *   The acccess result.
   */
  public function checkAccess(Editor $editor, AccountProxy $account) {
    return AccessResult::allowedIfHasPermission($account, 'use text format ' . $editor->getFilterFormat()->id());
  }

}
