<?php

namespace Drupal\hubspot_forms\Form;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\editor\Ajax\EditorDialogSave;
use Drupal\hubspot_forms\HubspotFormsCore;

/**
 * Ckeditor dialog form to insert Hubspot Form form.
 */
class CKEditor5DialogForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ckeditor5_hubspotforms_dialog_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $uuid = NULL) {
    $request = $this->getRequest();

    if ($uuid) {
      $form['uuid'] = [
        '#type' => 'value',
        '#value' => $uuid,
      ];
    }

    $hubspot_form_id = $request->get('hubspot_form_id') ? Xss::filter($request->get('hubspot_form_id')) : NULL;
    $hubspot_portal_id = $request->get('hubspot_portal_id') ? Xss::filter($request->get('hubspot_portal_id')) : NULL;
    if (!empty($hubspot_portal_id)) {
      $hubspot_form_id = $hubspot_portal_id . '::' . $hubspot_form_id;
    }

    $hubspotFormsCore = new HubspotFormsCore();
    $form['formid'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Hubspot Form'),
      '#description'   => $this->t('Please choose a form you would like to display.'),
      '#default_value' => $hubspot_form_id,
      '#options'       => $hubspotFormsCore->getFormIds(),
      '#required'      => TRUE,
    ];

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => !empty($hubspot_form_id) ? $this->t('Update') : $this->t('Insert'),
        '#button_type' => 'primary',
        '#ajax' => [
          'callback' => [$this, 'ajaxSubmitForm'],
        ],
      ],
    ];

    return $form;
  }

  /**
   * Ajax submit callback to insert or replace the html in ckeditor.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse|array
   *   Ajax response for injecting html in ckeditor.
   */
  public static function ajaxSubmitForm(array &$form, FormStateInterface $form_state) {
    [$hubspot_portal_id, $hubspot_form_id] = explode('::', $form_state->getValue('formid'));
    $response = new AjaxResponse();
    $response->addCommand(new EditorDialogSave([
      'attributes' => [
        'data-form-id' => !empty($hubspot_form_id) ? Xss::filter($hubspot_form_id) : 0,
        'data-portal-id' => !empty($hubspot_portal_id) ? Xss::filter($hubspot_portal_id) : 0,
      ],
    ]));
    $response->addCommand(new CloseModalDialogCommand());
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Required but not used.
  }

}
