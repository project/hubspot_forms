<?php

namespace Drupal\hubspot_forms\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Configure site information settings for this site.
 */
class HubspotSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'hubspot_forms_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['hubspot_forms.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('hubspot_forms.settings');

    $form['hubspot_access_type'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Hubspot Access Type'),
      '#default_value' => $config->get('hubspot_access_type'),
      '#options'   => [
        0 => $this->t('API Key (Legacy)'),
        1 => $this->t('Access Token'),
      ],
    ];

    $form['hubspot_api_key'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Hubspot API Key (Legacy method)'),
      '#default_value' => $config->get('hubspot_api_key'),
      '#description'   => $this->t('Please use <strong>demo</strong> to load example forms. Generate a @newkey. Make sure to clear Drupal cache after you change API Key. <strong>Warning</strong>: @keysunset.', [
        '@newkey' => Link::fromTextAndUrl($this->t('new key'), Url::fromUri('https://app.hubspot.com/keys/get'))->toString(),
        '@keysunset' => Link::fromTextAndUrl($this->t('API Key Sunset'), Url::fromUri('https://developers.hubspot.com/changelog/upcoming-api-key-sunset'))->toString(),
      ]),
      '#states' => [
        'visible' => [':input[name="hubspot_access_type"]' => ['value' => 0]],
        'required' => [':input[name="hubspot_access_type"]' => ['value' => 0]],
      ],
    ];

    $form['hubspot_access_token'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Hubspot Access Token'),
      '#default_value' => $config->get('hubspot_access_token'),
      '#description'   => $this->t('Provide Private App access token. Refer to the @privateappsdocs.', [
        '@privateappsdocs' => Link::fromTextAndUrl($this->t('Private Apps documentation'), Url::fromUri('https://developers.hubspot.com/docs/api/private-apps'))->toString(),
      ]),
      '#states' => [
        'visible' => [':input[name="hubspot_access_type"]' => ['value' => 1]],
        'required' => [':input[name="hubspot_access_type"]' => ['value' => 1]],
      ],
    ];

    $form['hubspot_portal_id'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Hubspot Portal ID'),
      '#default_value' => $config->get('hubspot_portal_id'),
      '#description'   => $this->t('In your HubSpot account, navigate to Dashboard'),
      '#states' => [
        'visible' => [':input[name="hubspot_access_type"]' => ['value' => 1]],
        'required' => [':input[name="hubspot_access_type"]' => ['value' => 1]],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('hubspot_forms.settings');

    // Enable/Disable plugin.
    $config->set('hubspot_access_type', $form_state->getValue('hubspot_access_type'))->save();
    $config->set('hubspot_api_key', $form_state->getValue('hubspot_api_key'))
      ->save();
    $config->set('hubspot_access_token', $form_state->getValue('hubspot_access_token'))->save();
    $config->set('hubspot_portal_id', $form_state->getValue('hubspot_portal_id'))->save();

    // Clear Drupal cache.
    \Drupal::cache()->delete('hubspot_forms');

    parent::submitForm($form, $form_state);
  }

}
