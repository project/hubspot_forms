<?php

namespace Drupal\hubspot_forms;

use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\ClientException;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Core HubspotForms class.
 *
 * @package Drupal\hubspot_forms
 */
class HubspotFormsCore {

  use StringTranslationTrait;

  /**
   * Get form ids.
   */
  public function getFormIds() {
    $cid = 'hubspot_forms';
    if ($cache = \Drupal::cache()->get($cid)) {
      $forms = $cache->data;
    }
    else {
      $forms = $this->fetchHubspotForms();
      \Drupal::cache()->set($cid, $forms);
    }
    $form_ids = [
      '' => $this->t('Choose a Hubspot form'),
    ];
    if (!empty($forms)) {
      $form_ids = array_merge($form_ids, $forms);
    }
    return $form_ids;
  }

  /**
   * Make call to Hubspot Forms API and get a list of all available forms.
   */
  public function fetchHubspotForms() {
    $forms = [];
    $config = \Drupal::config('hubspot_forms.settings');
    $access_type = trim($config->get('hubspot_access_type'));

    if (!empty($access_type)) {
      $access_token = $config->get('hubspot_access_token');
      $fetchedForms = $this->fetchFormsByAccessToken($access_token);
      usort($fetchedForms, function ($a, $b) {
        return strtotime($b->createdAt) - strtotime($a->createdAt);
      });
      $portal_id = trim($config->get('hubspot_portal_id'));
      foreach ($fetchedForms as $form) {
        $forms[$portal_id . '::' . $form->id] = $form->name;
      }
    }
    else {
      // Use Legacy method.
      $api_key = $config->get('hubspot_api_key');
      $fetchedForms = $this->fetchFormsByApiKey($api_key);
      usort($fetchedForms, function ($a, $b) {
        return $b->createdAt - $a->createdAt;
      });
      if (!empty($fetchedForms)) {
        foreach ($fetchedForms as $item) {
          $forms[$item->portalId . '::' . $item->guid] = $item->name;
        }
      }
    }

    return $forms;
  }

  /**
   * Fetch forms using Access Token.
   *
   * @param string $access_token
   *   Access Token.
   *
   * @return array
   *   Array of Forms
   */
  private function fetchFormsByAccessToken($access_token) {

    // Get all forms from a portal.
    // See https://developers.hubspot.com/docs/api/marketing/forms
    $uri = 'https://api.hubapi.com/marketing/v3/forms/';
    $client = \Drupal::httpClient();
    $options = ['limit' => 100];
    $more_forms = false;
    $results = [];
    do {
      try {
        $request = $client->request('GET', $uri, [
          'query' => $options,
          'timeout' => 5,
          'headers' => [
            'Accept' => 'application/json',
            'authorization' => "Bearer $access_token",
          ],
        ]);

        if ($request->getStatusCode() == 200) {
          $response = json_decode($request->getBody());
          if (!empty($response) && !empty($response->results)) {
            $results = array_merge($results, $response->results);
          }
          if (property_exists($response, 'paging')) {
            $options['after'] = $response->paging->next->after;
            $more_forms = TRUE;
          }
          else {
            $more_forms = FALSE;
          }
        }
      }
      catch (ClientException $e) {
        $message = $e->getMessage() . '. Make sure you provided correct Hubspot Access Token on the configuration page.';
        \Drupal::logger('hubspot_forms')->notice($message);
        break;
      }
      catch (ConnectException $e) {

        $message = $e->getMessage();
        \Drupal::logger('hubspot_forms')->notice($message);
        break;
      }
    } while ($more_forms);
    return $results;
  }

  /**
   * Fetch forms using API KEY.
   *
   * @param string $api_key
   *   API Key.
   *
   * @return array
   *   Array of Forms
   */
  private
  function fetchFormsByApiKey($api_key) {
    try {
      // Get all forms from a portal.
      // See http://developers.hubspot.com/docs/methods/forms/v2/get_forms
      $uri = 'https://api.hubapi.com/forms/v2/forms?hapikey=' . $api_key;
      $client = \Drupal::httpClient();
      $request = $client->request('GET', $uri,
        ['timeout' => 5, 'headers' => ['Accept' => 'application/json']]);
      if ($request->getStatusCode() == 200) {
        $response = json_decode($request->getBody());
        if (empty($response)) {
          return [];
        }
        else {
          return $response;
        }
      }
      else {
        return [];
      }
    } catch (ClientException $e) {
      $message = $e->getMessage() . '. Make sure you provided correct Hubspot API Key on the configuration page.';
      \Drupal::logger('hubspot_forms')->notice($message);
      return [];
    } catch (ConnectException $e) {
      $message = $e->getMessage();
      \Drupal::logger('hubspot_forms')->notice($message);
      return [];
    }
  }

  /**
   * Check Hubspot connection.
   */
  public
  function isConnected() {
    $forms = $this->fetchHubspotForms();
    return count($forms);
  }

}
