<?php

namespace Drupal\hubspot_forms\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\hubspot_forms\HubspotFormsCore;

/**
 * Plugin implementation of the 'field_hubspot_form_formatter_label' formatter.
 *
 * @FieldFormatter(
 *   id = "field_hubspot_form_formatter_label",
 *   module = "hubspot_forms",
 *   label = @Translation("Display Hubspot form label"),
 *   field_types = {
 *     "field_hubspot_form"
 *   }
 * )
 */
class HubspotFormFormatterLabel extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $HubspotFormsCore = new HubspotFormsCore();

    // Get all forms.
    $forms = $HubspotFormsCore->getFormIds();

    foreach ($items as $delta => $item) {
      // Get selected form ID.
      $form_id = $item->get('form_id')->getValue();

      if ($HubspotFormsCore->isConnected()) {
        $elements[$delta] = [
          '#type' => 'markup',
          '#markup' => $forms[$form_id],
        ];
      }
      else {
        $elements[$delta] = [
          '#markup' => $this->t('Please provide a valid Hubspot API key.'),
        ];
      }
    }

    return $elements;
  }

}
