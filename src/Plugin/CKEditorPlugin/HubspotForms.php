<?php

namespace Drupal\hubspot_forms\Plugin\CKEditorPlugin;

use Drupal\ckeditor\CKEditorPluginInterface;
use Drupal\ckeditor\CKEditorPluginButtonsInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\editor\Entity\Editor;

/**
 * Defines the "HubspotForms" plugin.
 *
 * @CKEditorPlugin(
 *   id = "hubspot_forms",
 *   label = @Translation("Hubspot Forms")
 * )
 */
class HubspotForms extends PluginBase implements CKEditorPluginInterface, CKEditorPluginButtonsInterface {
  use StringTranslationTrait;

  /**
   * The module extension list.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected $moduleExtensionList;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->moduleExtensionList = \Drupal::service('extension.list.module');
  }

  /**
   * {@inheritdoc}
   */
  public function getDependencies(Editor $editor) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLibraries(Editor $editor) {
    return [
      'core/drupal.ajax',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isInternal() {
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    return $this->moduleExtensionList->getPath('hubspot_forms') . '/assets/hubspot_forms.js';
  }

  /**
   * {@inheritdoc}
   */
  public function getButtons() {
    return [
      'hubspot_forms' => [
        'label' => $this->t('Hubspot Forms'),
        'image' => $this->moduleExtensionList->getPath('hubspot_forms') . '/assets/icon.png',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig(Editor $editor) {
    return [
      'hubspot_forms_dialog_title' => $this->t('Hubspot Forms'),
    ];
  }

}
