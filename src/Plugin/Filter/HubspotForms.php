<?php

namespace Drupal\hubspot_forms\Plugin\Filter;

use Drupal\Component\Utility\Html;
use Drupal\Component\Utility\Random;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Render\Renderer;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Render\RenderContext;
use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Render Hubspot Forms.
 *
 * @Filter(
 *   id = "hubspot_forms",
 *   title = @Translation("Hubspot Forms"),
 *   description = @Translation("Converts &#60;hubspotform&#62; to a Hubspot Form."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_TRANSFORM_REVERSIBLE,
 *   weight = 100,
 * )
 */
class HubspotForms extends FilterBase implements ContainerFactoryPluginInterface, TrustedCallbackInterface {

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Renderer $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('renderer'));
  }

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if (stristr($text, '<hubspotform') === FALSE && stristr($text, '[hubspot-form:') === FALSE) {
      return new FilterProcessResult($text);
    }

    $random = new Random();

    if (preg_match_all('/\[hubspot\-form(\:(.+))?( .+)?\]/isU', $text, $matches_code)) {
      foreach ($matches_code[0] as $ci => $code) {
        $form = [
          'form_id'   => $matches_code[2][$ci],
        ];
        // Override default attributes.
        if (!empty($matches_code[3][$ci]) && preg_match_all('/\s+([a-zA-Z_]+)\:(\s+)?([0-9a-zA-Z\/]+)/i', $matches_code[3][$ci], $matches_attributes)) {
          foreach ($matches_attributes[0] as $ai => $attribute) {
            $form[$matches_attributes[1][$ai]] = $matches_attributes[3][$ai];
          }
        }
        $element = [
          '#theme' => 'hubspot_form',
          '#target' => Html::getUniqueId('hubspotform-' . $random->string() . '-' . $form['form_id']),
          '#portal_id' => $form['portal_id'],
          '#form_id' => $form['form_id'],
        ];
        $replacement = $this->renderer->render($element);
        $text = str_replace($code, $replacement, $text);
      }
    }

    $result = new FilterProcessResult($text);
    $dom = Html::load($text);
    $xpath = new \DOMXPath($dom);
    foreach ($xpath->query('//hubspotform') as $element) {
      $form_id = $element->getAttribute('data-form-id');
      $portal_id = $element->getAttribute('data-portal-id');

      // Delete the consumed attributes.
      $element->removeAttribute('data-form-id');
      $element->removeAttribute('data-portal-id');

      $build = [];
      $build['element'] = [
        '#theme' => 'hubspot_form',
        '#target' => Html::getUniqueId('hubspotform-' . $random->string() . '-' . $form_id),
        '#portal_id' => $portal_id,
        '#form_id' => $form_id,
      ];
      $this->renderIntoDomNode($build, $element, $result);
    }
    $result->setProcessedText(Html::serialize($dom));
    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [];
  }

  /**
   * Renders the given render array into the given DOM node.
   */
  protected function renderIntoDomNode(array $build, \DOMNode $node, FilterProcessResult &$result) {
    $markup = $this->renderer->executeInRenderContext(new RenderContext(), function () use (&$build) {
      return $this->renderer->render($build);
    });
    $result = $result->merge(BubbleableMetadata::createFromRenderArray($build));
    static::replaceNodeContent($node, $markup);
  }

  /**
   * Replaces the contents of a DOMNode.
   */
  protected static function replaceNodeContent(\DOMNode &$node, $content) {
    if (strlen($content)) {
      $replacement_nodes = Html::load($content)->getElementsByTagName('body')
        ->item(0)
        ->childNodes;
    }
    else {
      $replacement_nodes = [$node->ownerDocument->createTextNode('')];
    }
    foreach ($replacement_nodes as $replacement_node) {
      $replacement_node = $node->ownerDocument->importNode($replacement_node, TRUE);
      $node->parentNode->insertBefore($replacement_node, $node);
    }
    $node->parentNode->removeChild($node);
  }

}
